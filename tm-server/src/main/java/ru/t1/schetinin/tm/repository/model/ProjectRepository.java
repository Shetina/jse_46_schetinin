package ru.t1.schetinin.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.schetinin.tm.api.repository.model.IProjectRepository;
import ru.t1.schetinin.tm.dto.model.ProjectDTO;
import ru.t1.schetinin.tm.model.Project;

import javax.persistence.EntityManager;

public class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public Class<Project> getEntity() {
        return Project.class;
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) throws Exception {
        @NotNull final Project project = new Project(name, description);
        return add(userId, project);
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId, @NotNull final String name) throws Exception {
        @NotNull final Project project = new Project(name);
        return add(userId, project);
    }

}